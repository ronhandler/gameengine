#include "state.h"
#include <chrono>

State::State()
:   engineRunning(true),
    frameNumber(0),
    /*
     *width(800),
     *height(500)
     */
    width(500),
    height(600),
    delta(std::chrono::seconds(0))
{

}
