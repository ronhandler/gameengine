#include "main.h"
#include "common_utils/files.h"

#include <iostream>

int main(int argc, char *argv[])
{
    // This is used once to extract the executable path from argv.
    CommonUtils::setExecutablePath(argc, argv);

    GameEngine engine;
    engine.Init();
    engine.Run();

    return 0;
}

