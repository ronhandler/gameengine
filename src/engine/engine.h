#ifndef ENGINE_H
#define ENGINE_H

#include "state.h"
#include "graphics.h"
#include "input.h"

#include <functional>

class Engine {
public:
    Engine();
    ~Engine();
    void Run();
    virtual void Init();

protected:
    State *_state;
    Input *_input;
    Renderer *_renderer;

    std::function<void(void)> _renderFunc;
    std::function<void(void)> _keyEventHandler;
};

/*!
 *
 * When deriving from Engine, we need to define the Init function.
 * Inside Init we should be able to setup _renderFunc and
 * _keyEventHandler.
 *
 */
class GameEngine : public Engine
{
public:
    virtual void Init();
};

#endif

