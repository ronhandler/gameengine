#ifndef STATE_H
#define STATE_H

#include <chrono>

class State {
public:
    State();
    bool engineRunning;
    unsigned int frameNumber;

    int height;
    int width;

    std::chrono::duration<double> delta;
};

#endif

