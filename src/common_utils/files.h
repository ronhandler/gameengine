#ifndef FILES_H
#define FILES_H

#include <string>

//#include <boost/filesystem.hpp>
//namespace fs = boost::filesystem;
namespace CommonUtils
{

//extern fs::path execPath;
void setExecutablePath(int argc, char *argv[]);
std::string getExecutablePath();
std::string getExecutableDir();
std::string expandPath(const std::string &p);
std::string readFile(const std::string &fn);

// Helper class to convert std::string to array of char arrays.
struct StringToCharArrayArray {
  const char *p;
  StringToCharArrayArray(const std::string& s) : p(s.c_str()) {}
  operator const char**() { return &p; }
};

} // Namespace.

#endif

