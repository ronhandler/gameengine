#include "graphics.h"
#include "../common_utils/files.h"

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>

Renderer::Renderer()
:   _state(nullptr)
{
}

void Renderer::linkState(State *state)
{
    _state = state;
}

int Renderer::Init()
{
    return 0;
}
void Renderer::renderFrame()
{
    std::cout << "Rendering frame " << _state->frameNumber << "..." << std::endl;
    (_state->frameNumber)++;
}

GLFWRenderer::~GLFWRenderer()
{
    for (int i=0; i<_meshes.size(); i++)
    {
        glDeleteProgram(_meshes[i]->getProgram());
        _meshes[i]->setProgram(0);
    }

    // Terminate GLFW, clearing any resources allocated by GLFW.
    glfwTerminate();
}

int GLFWRenderer::Init()
{
    // Need to link a state first.
    if (_state == nullptr) {
        std::cerr << "Error initializing renderer: State is NULL." << std::endl;
        return 1;
    }

    GLint width = _state->width;
    GLint height = _state->height;

    // Init GLFW
    glfwInit();
    // Set all the required options for GLFW
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Create a GLFWwindow object that we can use for GLFW's functions
    _window = glfwCreateWindow(width, height, "Main window", nullptr, nullptr);    
    if (_window == nullptr)
    {
        std::cerr << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(_window);


    // Save the state pointer to be used inside static member functions.
    glfwSetWindowUserPointer(_window, _state);

    // Register some window callbacks.
    glfwSetFramebufferSizeCallback(_window, GLFWRenderer::resizeWindowCallback);
    glfwSetWindowCloseCallback(_window, GLFWRenderer::closeWindowCallback);


    // Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
    glewExperimental = GL_TRUE;
    // Initialize GLEW to setup the OpenGL Function pointers
    if (glewInit() != GLEW_OK)
    {
        std::cerr << "Failed to initialize GLEW" << std::endl;
        return -1;
    }    

    // Define the viewport dimensions
    glfwGetFramebufferSize(_window, &width, &height);  
    glViewport(0, 0, width, height);

    // Reset The Projection Matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //gluPerspective(30.0f,(GLfloat)width/(GLfloat)height,0.3f,200.0f);
    glMatrixMode(GL_MODELVIEW);

    registerMesh(new Mesh(
        {
             // Xyz-Positions     // Uv-coords
            -0.5f, -0.5f,  0.0f,  0.0,  0.0,
            -0.5f,  0.5f,  0.0f,  0.0,  1.0,
             0.5f, -0.5f,  0.0f,  1.0,  0.0,
             0.5f,  0.5f,  0.0f,  1.0,  1.0,
        },
        "$RESOURCES/textures/image.png")
    );


    //glFrontFace(GL_CW);
    //glCullFace(GL_BACK);
    //glEnable(GL_CULL_FACE);
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    return 0;
}

void GLFWRenderer::closeWindowCallback(GLFWwindow* window)
{
    // Load the state (since we're inside a static member function).
    State *state = (State *) glfwGetWindowUserPointer(window);
    state->engineRunning = false;
}

void GLFWRenderer::resizeWindowCallback(GLFWwindow* window, int width, int height)
{
    State *state = (State *) glfwGetWindowUserPointer(window);
    state->height = height;
    state->width = width;
    glViewport(0, 0, width, height);
}

GLuint GLFWRenderer::createShaderProgram(const std::string &vs, const std::string &fs)
{
    GLuint program = 0;
    GLint success = 0;
    const GLint buff_size = 512;
    char infoLog[buff_size];

    GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, CommonUtils::StringToCharArrayArray(vs), nullptr);
    glCompileShader(vertex_shader);
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vertex_shader, buff_size, nullptr, infoLog);
        std::cerr << "Error compiling vertex shader: " << infoLog << std::endl;
        return 0;
    }
    GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, CommonUtils::StringToCharArrayArray(fs), nullptr);
    glCompileShader(fragment_shader);
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fragment_shader, buff_size, nullptr, infoLog);
        std::cerr << "Error compiling fragment shader: " << infoLog << std::endl;
        return 0;
    }
    program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(program, buff_size, nullptr, infoLog);
        std::cerr << "Error linking program: " << infoLog << std::endl;
        return 0;
    }

    glDetachShader(program, vertex_shader);
    glDeleteShader(vertex_shader);
    glDetachShader(program, fragment_shader);
    glDeleteShader(fragment_shader);

    return program;
}

void GLFWRenderer::registerMesh(Mesh *mesh)
{
    if (mesh == nullptr)
        return;

    // Load the shaders.
    std::string vs_text = CommonUtils::readFile("$RESOURCES/shaders/2D_textured_rectangle_vs.glsl");
    std::string fs_text = CommonUtils::readFile("$RESOURCES/shaders/2D_textured_rectangle_fs.glsl");

    GLuint progId = createShaderProgram(vs_text, fs_text);

    mesh->setProgram(progId);

    // Create a VAO.
    GLuint vao = 0;
    glGenVertexArrays(1, &vao);
    mesh->setVAO(vao);

    GLuint vbo = 0;
    glGenBuffers(1, &vbo);

    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, mesh->getVertices().size()*sizeof(GLfloat), mesh->getVertices().data(), GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (GLvoid*)0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (GLvoid*)(3*sizeof(GLfloat)));

    for (int i=0; i<2; i++)
        glEnableVertexAttribArray(i);

    // Unbind the VBO.
    glBindBuffer(GL_ARRAY_BUFFER, 0); 

    // Bind the texture.
    glGenTextures(1, &(mesh->getTexture()->id));
    glBindTexture(GL_TEXTURE_2D, mesh->getTexture()->id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    GLuint h = mesh->getTexture()->getHeight();
    GLuint w = mesh->getTexture()->getWidth();
    GLubyte *data = mesh->getTexture()->getDataPtr();
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);

    _meshes.push_back(mesh);

}

void GLFWRenderer::renderFrame()
{
    // Clear the color and depth buffers.
    glClearColor(0.6f, 0.6f, 0.6f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    for (int i=0; i<_meshes.size(); i++)
    {
        glUseProgram(_meshes[i]->getProgram());

        // Bind the texture.
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, _meshes[i]->getTexture()->id);
        // Bind the VAO and draw it.
        glBindVertexArray(_meshes[i]->getVAO());

        glDrawArrays(GL_TRIANGLE_STRIP, 0, _meshes[i]->getVertices().size()/5);
    }

    // Swap the screen buffers
    glfwSwapBuffers(_window);

    //std::cout << "Delta = " << _state->delta.count() << std::endl;
}

