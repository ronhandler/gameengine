#ifndef INPUT_H
#define INPUT_H

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "state.h"

class Input {
public:
    Input();
    void linkState(State *state);
    virtual void handleInput();
protected:
    State *_state;
};

class GLFWInput : public Input {
public:
    virtual void handleInput();

    void registerKeyEventHandler(GLFWwindow *window);
    static void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods);

};

#endif


