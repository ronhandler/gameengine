#ifndef GRAPHICS_H
#define GRAPHICS_H

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <vector>
#include <memory>

#include "state.h"
#include "texture.h"
#include "mesh.h"

class Renderer {
public:
    Renderer();
    void linkState(State *state);
    virtual int Init();
    virtual void renderFrame();
protected:
    State *_state;
};

class GLFWRenderer : public Renderer {
public:
    ~GLFWRenderer();

    virtual int Init();

    GLFWwindow *getWindow() { return _window; }

    static void closeWindowCallback(GLFWwindow* window);
    static void resizeWindowCallback(GLFWwindow* window, int width, int height);
    GLuint createShaderProgram(const std::string &vs, const std::string &fs);
    void registerMesh(Mesh *mesh);
    virtual void renderFrame();

protected:
    GLFWwindow *_window;

    std::vector<GLfloat> _vertices;

    std::vector<Mesh *> _meshes;

};

#endif

