#version 330 core

// has to have same name as vertex shader
in vec2 uv_frag;

// our texture
uniform sampler2D tex;

// actual output
// gl_FragColor is deprecated
out vec4 frag_color;

void main(){
    vec4 tex_color = texture(tex, uv_frag);
    /*
    if (tex_color.a <= 0.1)
        discard;
    */
    frag_color = tex_color;
}
