#include "engine.h"

#include <iostream>
using namespace std;
#include <chrono>

/*
 *#include <thread>
 *#include <chrono>
 */

Engine::~Engine()
{
    delete _state;
    delete _input;
    delete _renderer;
}

Engine::Engine()
{
}

void Engine::Init()
{
    _state = new State();
    _input = new Input();

    _renderer = new Renderer();

    _keyEventHandler = [this](void)
    {
        _input->handleInput();
    };
    _renderFunc = [this](void)
    {
        _renderer->renderFrame();
    };
}

void Engine::Run()
{
    std::chrono::system_clock::time_point t0;
    std::chrono::system_clock::time_point t1;
    std::chrono::duration<double> oneSec(0.0);

    unsigned int fps = 0;
    // Main loop
    while (_state->engineRunning)
    {
        fps++;
        t0 = std::chrono::system_clock::now();

        _keyEventHandler();
        _renderFunc();

        t1 = std::chrono::system_clock::now();
        _state->delta = t1 - t0;
        oneSec += _state->delta;
        if (oneSec > std::chrono::seconds(1))
        {
            //std::cout << "Time elapsed: " << _state->delta.count() << std::endl;
            std::cout << "FPS: " << fps << std::endl;
            oneSec -= std::chrono::seconds(1);
            fps = 0;
        }
    }
}

void GameEngine::Init()
{
    _state = new State();

    _renderer = new GLFWRenderer();
    _renderer->linkState(_state);

    _renderer->Init();

    _input = new GLFWInput();
    _input->linkState(_state);

    ((GLFWInput *)_input)->registerKeyEventHandler(
        ((GLFWRenderer *)_renderer)->getWindow()
    );

    _keyEventHandler = [this](void)
    {
        _input->handleInput();
    };
    _renderFunc = [this](void)
    {
        _renderer->renderFrame();
    };
}
