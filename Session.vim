let SessionLoad = 1
if &cp | set nocp | endif
let s:so_save = &so | let s:siso_save = &siso | set so=0 siso=0
let v:this_session=expand("<sfile>:p")
let ConqueTerm_PyExe = ""
let ConqueTerm_SendFileKey = "<F10>"
let ConqueTerm_ColorMode = ""
let ConqueGdb_GdbExe = ""
let ConqueTerm_EscKey = "<Esc>"
let NERDSpaceDelims = "0"
let NERDLPlace = "[>"
let NERDDefaultAlign = "none"
let NERDCommentEmptyLines = "0"
let ConqueTerm_Color =  1 
let ConqueGdb_ToggleBreak = "<Leader>b"
let ConqueTerm_Version =  230 
let ConqueTerm_CloseOnEnd =  0 
let ConqueTerm_ShowBell =  0 
let ConqueTerm_SendVisKey = "<F9>"
let NERDUsePlaceHolders = "1"
let NERDCreateDefaultMappings = "1"
let ConqueTerm_InsertOnEnter =  0 
let ConqueTerm_PromptRegex = "^\\w\\+@[0-9A-Za-z_.-]\\+:[0-9A-Za-z_./\\~,:-]\\+\\$"
let ConqueTerm_SessionSupport =  0 
let NERDCompactSexyComs = "0"
let ConqueGdb_SrcSplit = "above"
let ConqueTerm_FastMode =  0 
let ConqueTerm_TERM = "vt100"
let RunFile = "./a.out"
let NERDRemoveAltComs = "1"
let NERDAllowAnyVisualDelims = "1"
let ConqueTerm_CWInsert =  0 
let ConqueGdb_Next = "<Leader>n"
let ConqueGdb_Print = "<Leader>p"
let ConqueTerm_SendFunctionKeys =  0 
let NERDCommentWholeLinesInVMode = "0"
let ConqueTerm_Interrupt = "<C-c>"
let NERDTrimTrailingWhitespace = "0"
let NERDMenuMode = "3"
let ConqueTerm_Idx =  0 
let NERDDefaultNesting = "1"
let ConqueGdb_Leader = "<Leader>"
let NERDRPlace = "<]"
let ConqueTerm_UnfocusedUpdateTime =  500 
let ConqueGdb_SaveHistory =  0 
let ConqueTerm_TermLoaded =  1 
let ConqueGdb_Run = "<Leader>r"
let ConqueGdb_Step = "<Leader>s"
let ConqueTerm_TerminalsString = ""
let ConqueTerm_ExecFileKey = "<F11>"
let ConqueGdb_ReadTimeout =  50 
let ConqueTerm_InsertCharPre =  0 
let NERDRemoveExtraSpaces = "0"
let ConqueGdb_Finish = "<Leader>f"
let ConqueGdb_Continue = "<Leader>c"
let ConqueTerm_FocusedUpdateTime =  80 
let ConqueTerm_CodePage =  0 
let ConqueTerm_Loaded =  1 
let ConqueTerm_Syntax = "conque_term"
let NERDBlockComIgnoreEmpty = "0"
let ConqueGdb_Disable =  0 
let ConqueTerm_ToggleKey = "<F8>"
let ConqueTerm_StartMessages =  1 
let ConqueGdb_Backtrace = "<Leader>t"
let ConqueTerm_PyVersion =  2 
let ConqueTerm_ReadUnfocused =  0 
silent only
silent tabonly
cd ~/Projects/gameengine/build
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
badd +1 ../src/engine/graphics.cpp
badd +1 ../src/engine/graphics.h
badd +1 ../CMakeLists.txt
badd +1 ../src/engine/engine.cpp
badd +1 ../src/engine/engine.h
badd +1 ../src/engine/state.cpp
badd +1 ../src/engine/state.h
badd +1 ../src/engine/texture.cpp
badd +1 ../src/engine/texture.h
badd +1 ../src/engine/mesh.cpp
badd +1 ../src/engine/mesh.h
argglobal
silent! argdel *
set stal=2
tabnew
tabnew
tabnew
tabnew
tabnew
tabnew
tabnew
tabnew
tabnew
tabnew
tabnext -10
edit ../src/engine/graphics.cpp
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 98 - ((21 * winheight(0) + 14) / 28)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
98
normal! 05|
tabnext
edit ../src/engine/graphics.h
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 12 - ((11 * winheight(0) + 14) / 28)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
12
normal! 0
tabnext
edit ../src/engine/mesh.cpp
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 19 - ((18 * winheight(0) + 14) / 28)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
19
normal! 038|
tabnext
edit ../src/engine/mesh.h
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 12 - ((11 * winheight(0) + 14) / 28)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
12
normal! 05|
tabnext
edit ../src/engine/texture.cpp
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 7 - ((6 * winheight(0) + 14) / 28)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
7
normal! 03|
tabnext
edit ../src/engine/texture.h
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 18 - ((12 * winheight(0) + 14) / 28)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
18
normal! 0
tabnext
edit ../src/engine/engine.cpp
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 1 - ((0 * winheight(0) + 14) / 28)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
1
normal! 0
tabnext
edit ../src/engine/engine.h
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 1 - ((0 * winheight(0) + 14) / 28)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
1
normal! 0
tabnext
edit ../src/engine/state.cpp
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 1 - ((0 * winheight(0) + 14) / 28)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
1
normal! 0
tabnext
edit ../src/engine/state.h
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 1 - ((0 * winheight(0) + 14) / 28)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
1
normal! 0
tabnext
edit ../CMakeLists.txt
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 14 - ((13 * winheight(0) + 14) / 28)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
14
normal! 05|
tabnext 1
set stal=1
if exists('s:wipebuf') && len(win_findbuf(s:wipebuf)) == 0
  silent exe 'bwipe ' . s:wipebuf
endif
unlet! s:wipebuf
set winheight=1 winwidth=20 shortmess=filnxtToO
set winminheight=1 winminwidth=1
let s:sx = expand("<sfile>:p:r")."x.vim"
if file_readable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &so = s:so_save | let &siso = s:siso_save
doautoall SessionLoadPost
unlet SessionLoad
" vim: set ft=vim :
