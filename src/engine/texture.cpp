#include "texture.h"
#include <png.h>
#include <assert.h>
#include <cstdio>
#include <cstring>

Texture::Texture()
: id(0)
{

}

Texture::~Texture()
{
    if (_data)
    {
        delete _data;
    }
}

PngTexture::PngTexture()
:   Texture()
{
    png_ptr = png_create_read_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    assert(png_ptr != nullptr);
    info_ptr = png_create_info_struct(png_ptr);
    assert(info_ptr != nullptr);
}

PngTexture::~PngTexture()
{
    if (_data)
        delete _data;

    _data = nullptr;
}

void PngTexture::Read(const std::string &filename)
{
    //std::string textureFile = CommonUtils::readFile(filename);

    FILE *fp = fopen(CommonUtils::expandPath(filename).c_str(), "rb");

    png_init_io(png_ptr, fp);

    int transforms = PNG_TRANSFORM_IDENTITY;
    //int transforms = PNG_TRANSFORM_IDENTITY | PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING | PNG_TRANSFORM_EXPAND;
 
    png_read_png(png_ptr, info_ptr, transforms, nullptr);

    int sink = 0;
    png_get_IHDR(png_ptr, info_ptr,
            &_width,
            &_height,
            &_bitDepth,
            &_colorType,
            &sink,  //&interlace_type,
            &sink,  //&compression_type,
            &sink); //&filter_method);
    _channels = png_get_channels(png_ptr, info_ptr);
    png_bytepp data = png_get_rows(png_ptr, info_ptr);

    unsigned int row_bytes = png_get_rowbytes(png_ptr, info_ptr);
    _data = (unsigned char*) malloc(row_bytes * _height);
 
    png_bytepp row_pointers = png_get_rows(png_ptr, info_ptr);
 
    for (unsigned int i = 0; i < _height; i++) {
        // note that png is ordered top to
        // bottom, but OpenGL expect it bottom to top
        // so the order or swapped
        memcpy(_data + row_bytes * (_height-1-i), row_pointers[i], row_bytes);
    }
 
    // Close the file after it's been read.
    fclose(fp);

    /*
     *std::cout << "Dimensions: " << _width << "x" << _height << std::endl;
     *std::cout << "BitDepth: " << _bitDepth << std::endl;
     *std::cout << ((_colorType == PNG_COLOR_TYPE_RGB_ALPHA) ? "RGBA" : "Other type") << std::endl;
     */

    // Clean up.
    //png_destroy_read_struct(&png_ptr, nullptr, nullptr);
    png_destroy_read_struct(&png_ptr, &info_ptr, nullptr);
}

unsigned char &PngTexture::at(unsigned int x, unsigned int y, int channel)
{
    return (_data[y*_height+x*_channels + channel]);
}
