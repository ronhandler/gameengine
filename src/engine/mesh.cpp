#include "mesh.h"

#include "texture.h"
#include "../common_utils/files.h"

Mesh::Mesh()
: _texture(nullptr)
{
}

// Constructor.
Mesh::Mesh(std::vector<float> vertices, const std::string &texture_path)
{
    _vertices = vertices;

    // TODO:
    // Consider creating a factory class that will dispatch the correct
    // Texture object based on the file extension...
    // For now we just use PngTexture.
    _texture = new PngTexture;
    _texture->Read(texture_path);
}

std::vector<float> & Mesh::getVertices()
{
    return _vertices;
}
Texture * Mesh::getTexture()
{
    return _texture;
}
unsigned int Mesh::getProgram()
{
    return _program;
}
unsigned int Mesh::getVAO()
{
    return _vao;
}

void Mesh::setVertices(const std::vector<float> &verts)
{
    _vertices = verts;
}
void Mesh::setTexture(Texture *tex)
{
    _texture = tex;
}
void Mesh::setProgram(unsigned int prog)
{
    _program = prog;
}
void Mesh::setVAO(unsigned int vao)
{
    _vao = vao;
}
