#include "files.h"
#include <regex>
#include <fstream>
#include <streambuf>

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

namespace CommonUtils
{

fs::path execPath;

void setExecutablePath(int argc, char *argv[])
{
    execPath = fs::canonical(argv[0]);
}

std::string getExecutablePath()
{
    return execPath.string();
}
std::string getExecutableDir()
{
    return execPath.parent_path().string();
}

std::string expandPath(const std::string &p)
{
    fs::path resourcesDir = execPath.parent_path();
    resourcesDir += fs::path("/resources");
    return std::regex_replace(p, std::regex("\\$RESOURCES"), resourcesDir.string());
}

std::string readFile(const std::string &fn)
{
    std::ifstream t(expandPath(fn));
    std::stringstream buffer;
    buffer << t.rdbuf();
    return buffer.str();
}

} // Namespace.

