#include "input.h"
#include <iostream>
using namespace std;

Input::Input()
:   _state(nullptr)
{
}

void Input::linkState(State *state)
{
    _state = state;
}

void Input::handleInput()
{
    cout << "Please type a command... (type \"quit\" to quit)" << endl;
    string input;
    cin >> input;
    if (input == "quit")
    {
        _state->engineRunning = false;
    }
}

void GLFWInput::handleInput()
{
    glfwPollEvents();
}

void GLFWInput::registerKeyEventHandler(GLFWwindow *window)
{
    // Save the context (State).
    // We save the state because the callback member function must be
    // static, and needs to be connected to the object somehow.
    glfwSetWindowUserPointer(window, _state);

    // Set the keyboard input callback function.
    glfwSetKeyCallback(window, GLFWInput::key_callback);
}

void GLFWInput::key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    // Load the state.
    State *state = (State *) glfwGetWindowUserPointer(window);

    if (action == GLFW_PRESS)
    {
        if (key == GLFW_KEY_ESCAPE || key == GLFW_KEY_Q)
        {
            state->engineRunning = false;
            glfwSetWindowShouldClose(window, GLFW_TRUE);
        }
    }
}

