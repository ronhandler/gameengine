#ifndef TEXTURE_H
#define TEXTURE_H

#include "../common_utils/files.h"
#include <iostream>

#include <png.h>

class Texture {
public:
    Texture();
    ~Texture();
    virtual void Read(const std::string &filename) = 0;
    virtual unsigned char &at(unsigned int x, unsigned int y, int channel) = 0;
    unsigned char *getDataPtr() const { return _data; }
    int getWidth() const { return _width; }
    int getHeight() const { return _height; }
    unsigned int id;
protected:
    unsigned char *_data;
    unsigned int _width;
    unsigned int _height;
    int _bitDepth;
    int _colorType;
    int _channels;
};


class PngTexture : public Texture {
public:
    PngTexture();
    ~PngTexture();

    void Read(const std::string &filename);
    unsigned char &at(unsigned int x, unsigned int y, int channel);

protected:
    png_structp png_ptr;
    png_infop info_ptr;
};

#endif

