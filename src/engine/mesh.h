#ifndef MESH_H
#define MESH_H

#include <vector>

#include "texture.h"
#include "../common_utils/files.h"

class Mesh {
public:
    Mesh();
    Mesh(std::vector<float> vertices, const std::string &texture_path);

    std::vector<float> & getVertices();
    Texture *getTexture();
    unsigned int getProgram();
    unsigned int getVAO();

    void setVertices(const std::vector<float> &verts);
    void setTexture(Texture *tex);
    void setProgram(unsigned int prog);
    void setVAO(unsigned int vao);
protected:
    std::vector<float> _vertices;
    Texture *_texture;
    unsigned int _program;
    unsigned int _vao;
};

#endif

